// Run some basic polyfills
// Timeout-able fetch + polyfil for non-promise non-fetch browsers.
// Rejects with new Error("timeout") when a timeout occurs.
window.req = function ( resourceName, jsonRequestBody ) {
    var TIMEOUT = 2500; // 2.5 seconds.
    if ( window.Promise && window.fetch ) {
        // Nice way.
        return new Promise( function ( resolve, reject ) {
            var timedout = false;
            var timeoutTimer = setTimeout( function failFetch() {
                timedout = true;
                reject( new Error( "timeout" ) );
            }, TIMEOUT);
            var fetchPromise = null;
            if ( jsonRequestBody ) {
                try {
                    jsonRequestBody = JSON.stringify( jsonRequestBody );
                }
                catch ( ex ) {
                    clearTimeout( timeoutTimer );
                    reject( "JSON.stringify: " + ex );
                }
                fetchPromise = fetch( resourceName, {
                    method: "POST",
                    headers: { "Content-Type": "application/json; charset=utf-8" },
                    body: jsonRequestBody
                } );
            }
            else { // default
                fetchPromise = fetch( resourceName );
            }
            fetchPromise.then( function ( response ) {
                clearTimeout( timeoutTimer );
                if ( timedout ) {
                    return; // Already rejected once.
                }
                response.json().then( resolve, reject );
            }, function ( rejectionMsg ) { 
                clearTimeout( timeoutTimer );
                if ( timedout ) { 
                    return;
                }
                reject( rejectionMsg );
            } );
        } );
    }
    else { // Polyfill
        var pendingCompleteFunction = null;
        var pendingProblemFunction = null;
        var oops = null;
        var done = false;
        var promiseFullfilled = false; // one one of resolved or reject must fire.
        var doneValue = null;
        var pretendPromise = /* Pretend Promise */ {
            then: function thenable ( callWhenComplete, callWhenProblem ) {
                if ( oops && callWhenProblem ) {
                    promiseFullfilled = true;
                    callWhenProblem( oops );
                    return;
                }
                if ( done && callWhenComplete ) {
                    promiseFullfilled = true;
                    callWhenComplete( doneValue );
                    return;
                }
                pendingCompleteFunction = callWhenComplete;
                pendingProblemFunction = callWhenProblem;
            }
        };
        var xhr = new XMLHttpRequest();
        try {
            jsonRequestBody = JSON.stringify( jsonRequestBody );
            xhr.open( ( jsonRequestBody ? "POST" : "GET" ), resourceName );
            xhr.timeout = TIMEOUT;
            xhr.onload = function ( eventArg ) {
                if ( promiseFullfilled ) { 
                    return; // Skip this! Already fullfilled the promise.
                }
                promiseFullfilled = true;
                var text = eventArg.target.responseText;
                try {
                    doneValue = JSON.parse( text );
                    done = true;
                    if ( pendingCompleteFunction ) {
                        pendingCompleteFunction( doneValue );
                    }
                }
                catch ( ex ) {
                    oops = ex;
                    if ( pendingProblemFunction ) {
                        pendingProblemFunction( ex );
                    }
                }
            }
            xhr.onerror = function ( eventArg ) {
                oops = eventArg;
                if ( pendingProblemFunction && !promiseFullfilled ) {
                    promiseFullfilled = true;
                    pendingProblemFunction( oops );
                }
            };
            xhr.ontimeout = function ( eventArg ) {
                oops = new Error( "timeout" );
                if ( pendingProblemFunction && !promiseFullfilled ) {
                    promiseFullfilled = true;
                    pendingProblemFunction( oops );
                }
            }
            if ( jsonRequestBody ) {
                xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                xhr.send( jsonRequestBody );
            }
            else {
                xhr.send();
            }
        }
        catch ( ex ) {
            oops = ex;
        }
        return pretendPromise;
    }
}
// Takes a set of promise/then pairs, followed by an optional reject function
// and ensures that the 'then' methods execute in the order specified (regardless 
// of the order the promises resolve in)
// Example: inorder( promise1, thenResolveFunc1, promise2, thenResolveFunc2, rejectFunc )
//  thenResolveFunc1 is gauranteed to execute before thenResolveFunc2, even if promise2 
//  resolves before promise1.
function parallelordered() {
    var endIndex = arguments.length;
    var rejectThenFunc = null;
    var earlyStop = false;
    var runReady = [];
    if ( arguments.length % 2 != 0 ) {
        // last argument is a generic error handler for the set.
        endIndex--;
        rejectThenFunc = arguments[ endIndex ]; // :-)  len - 1
    }
    // Fill result arrays..
    for ( var i = 0; i < endIndex; i += 2 ) {
        runReady.push( {
            thenFunc: arguments[ i + 1 ],
            fullfilled: false,
            isResolved: false,
            resolvedVal: undefined,
            rejectedVal: undefined
        } );
    }
    var posIndex = 0;
    var createTempThenFunc = function ( i ) {
        return function intermediateThenFunc ( thenResult ) {
            if ( earlyStop ) {
                return;
            }
            var ob = runReady[ i ];
            ob.fullfilled = true;
            ob.isResolved = true;
            ob.resolvedVal = thenResult;
            processThens();
        }
    }
    var createTempRejectFunc = function( i ) {
        return function intermediateRejectFunc( rejectResult ) {
            if ( earlyStop ) {
                return;
            }
            var ob = runReady[ i ];
            ob.fullfilled = true;
            ob.rejectedVal = rejectResult;
            processThens();
        }
    }
    var processThens = function () {
        while ( !earlyStop && posIndex < runReady.length && runReady[ posIndex ].fullfilled ) {
            var ob = runReady[ posIndex ];
            if ( ob.isResolved ) {
                ob.thenFunc( ob.resolvedVal );
            }
            else {
                if ( rejectThenFunc ) {
                    rejectThenFunc( ob.rejectedVal );
                }
                earlyStop = true;
            }
            posIndex++;
        }
        if ( earlyStop || posIndex == runReady.length ) {
            runReady = null;
        }
    }
    // Attach to promise thens (in reverse order)
    var reverseiter = runReady.length - 1;
    for ( var i = endIndex - 2; i >= 0; i -= 2 ) {
        arguments[ i ].then( createTempThenFunc( reverseiter ), createTempRejectFunc( reverseiter ) );
        reverseiter--;
    }
}
// Test for classList.toggle(string, force) - 'force' parameter support.
// Should force "test" off but only if it exists. Non-supporting implementations will 
// simply toggle "test" on (since it won't exist at first)
document.documentElement.classList.toggle( "test", false ); 
if ( document.documentElement.classList.contains( "test" ) ) {
    document.documentElement.classList.remove( "test" );
    // Needs to polyfill/replace existing 'toggle' implementation.
    // Find the current toggle API...
    var own = document.documentElement.classList;
    while ( !Object.getOwnPropertyDescriptor( own, "toggle" ) ) {
        own = Object.getPrototypeOf( own );
    }
    own.nativeToggle = own.toggle;
    var constructor = own.constructor;
    // replace it with a custom function that implements the second param
    own.toggle = function togglePolyfill( val, forceFlag ) {
        if ( arguments.length == 0 ) {
            throw new TypeError( "Argument not optional" );
        }
        if ( !( this instanceof constructor ) ) {
            throw new TypeError( "Invalid calling object" );
        }
        var stringVal = String( val );
        if ( arguments.length == 1 ) {
            return this.nativeToggle( val );
        }
        var boolForce = Boolean( forceFlag );
        if ( boolForce ) {
            this.add( stringVal );
            return true;
        }
        else {
            this.remove( stringVal );
            return false;
        }
    }
}

