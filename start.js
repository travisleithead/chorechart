/* Server startup */
const fs = require( 'fs' );
const http = require( 'http' );
const csvSyncParser = require( 'csv-parse/lib/sync' );

const DIRS = [ "./config" ];

function ensureDirs() {
    for ( let dir of DIRS ) {
        if ( !fs.existsSync( dir ) ) {
            fs.mkdirSync( dir );
        }
    }
}

const FILES = { // Relatively pathed...
    "users":  "./config/users.json",
    "groups": "./config/groups.json",
    "groups-sample" : "./config/groups_sample.json",
    "chores": "./config/chores.json",
    "csv":    "./chores.csv",
    "custom": "./config/availability.json",
    "custom-sample" : "./config/availability_sample.json",
    "state":  "./state.json"
};

const ERRORS = [
    /*0*/ `Server start-up error: configuration file '${ FILES[ "users" ] }' is invalid JSON. If this has been hand-edited, please fix JSON errors and try starting the server again.`,
    /*1*/ `Server start-up error: configuration file '${ FILES[ "groups" ] }' is invalid JSON. If this has been hand-edited, please fix JSON errors and try starting the server again.`,
    /*2*/ `Server start-up error: initial import of '${ FILES[ "csv" ] }' failed due to the following errors in the CSV file. Please correct them and start the server again:`,
    /*3*/ `Server start-up error: configuration file '${ FILES[ "chores" ] }' is invalid JSON. If this has been hand-edited, please fix JSON errors and try starting the server again.`,
    /*4*/ `Server start-up error: configuration file '${ FILES[ "users" ] }' has at least one object that is missing required keys: icon, name, alias, pin, admin, dailyMinPoints, deleted. Please restore the key name and value and start the server again.`,
    /*5*/ `Server start-up error: configuration file '${ FILES[ "groups" ] }' has at least one object that is missing required keys or has key type errors: alias (string), icon (string), nameSingular (string), namePlural (string), users (array). Please restore the key name and value and start the server again.`,
    /*6*/ `Server start-up error: configuration file '${ FILES[ "chores" ] }' has at least one object that is missing required keys: alias, icon, name, points, deleted. Please restore the key name and value and start the server again.`,
    /*7*/ `Server start-up error: configuration file '${ FILES[ "users" ] }' must be an array with at least one object (one user is required).`,
    /*8*/ `Server start-up error: configuration file '${ FILES[ "users" ] }' must have at least one user who is an admin (or no one will be able to approve chores)!`,
    /*9*/ `Server start-up error: configuration file '${ FILES[ "custom" ] }' is invalid JSON. If this has been hand-edited, please fix JSON errors and try starting the server again.`,
    /*10*/`Server start-up error: configuration file '${ FILES[ "custom" ] }' must have top level keys: 'availabilities' whose value is an array. Please restore the key name and values and start the server again.`,
    /*11*/`Server start-up error: configuration file '${ FILES[ "custom" ] }' has at least one object in the 'availabilities' array that is not an object. Please restore the key name and value and start the server again.`,
    /*12*/`Server start-up error: data file '${ FILES[ "state" ] }' is invalid JSON. This file should not be hand-edited, please fix JSON errors if possilble or delete the file and start the server again.`
];

const NOTES = [
    /*0*/ `Server start-up: NOTE, no '${ FILES[ "users" ] }' configuration file found (this may be your first time running the server). A sample file has been generated for you.`,
    /*1*/ `Server start-up: NOTE, no '${ FILES[ "groups" ] }' configuration file found (this may be your first time running the server). An empty groups file and sample file '${ FILES[ "groups-sample" ] }' has been generated for you to copy from.`,
    /*2*/ `Server start-up: NOTE, no '${ FILES[ "chores" ] }' or '${ FILES[ "csv" ] }' chore lists found (this may be your first time running the server). A sample chore list has been generated for you at '${ FILES[ "chores" ] }'. Modify it, or delete it and add a '${ FILES[ "csv" ] }' file instead with two columns (and no headers); first column should be the chore name, and the second column the number of points (a positive number) assigned to that chore.`,
    /*3*/ `Server start-up: NOTE, successfully imported '${ FILES[ "csv" ] }'. From now on this file will be ignored, as the chore data is now stored in '${ FILES[ "chores" ] }'. Feel free to delete '${ FILES[ "csv" ] }'.`,
    /*4*/ `Server start-up: NOTE, duplicate alias values in '${ FILES[ "users" ] }' found. These have been fixed as follows, but consider manually de-duping these instead:`,
    /*5*/ `Server start-up: NOTE, duplicate alias values in '${ FILES[ "groups" ] }' found. These have been fixed as follows, but consider manually de-duping these instead:`,
    /*6*/ `Server start-up: NOTE, duplicate alias values in '${ FILES[ "chores" ] }' found. These have been fixed as follows, but consider manually de-duping these instead:`,
    /*7*/ `Server start-up: NOTE, no '${ FILES[ "custom" ] }' configuration file found (this may be your first time running the server). An empty default file and sample file '${ FILES[ "custom-sample" ] }' has been generated for you to copy from.`,
    /*8*/ `Server start-up: NOTE, no '${ FILES[ "state" ] }' data file found (this may be your first time running the server). A default state file has been generated for you based on existing or newly created '${ FILES[ "users" ] }', '${ FILES[ "groups" ] }', and '${ FILES[ "chores" ] }' configuration files.`,
];

const SLOW_NETWORK = { // Artificial Server Delay to simulate slow networks
    DSL: 500, // ms
    CELL3G: 1200,
    CELL2G: 2000,
    CELL_SLOW: 5000,
    DIAL_UP: 9000
};

class UserManager {
    constructor() {
        if ( !fs.existsSync( FILES[ "users" ] ) ) {
            let defaultUsers = [ {
                icon: "defaultuser.png",
                name: "Administrator",
                alias: "admin", // must be unique!
                // groupId: "0", // (programmatically assigned if not provided or if value is bad. (See GroupManager)
                pin: "0000",
                admin: true, //  Su, M, T, W, Th, F, Sa
                dailyMinPoints: [ 0, 0, 0, 0, 0, 0, 0 ],
                deleted: false
            } ];
            fs.writeFileSync( FILES[ "users" ], JSON.stringify( defaultUsers, null, 4 ), { encoding: "utf8" } );
            console.log( NOTES[ 0 ] );
        }
        try { 
            var users = JSON.parse( fs.readFileSync( FILES[ "users" ], { encoding: "utf8" } ) );
        }
        catch ( ex ) {
            console.log( ERRORS[ 0 ] );
            process.exit();
        }
        // Min validation: expected keys are present and aliases forced to be unique.
        if ( !Array.isArray( users ) || users.length == 0 ) {
            console.log( ERRORS[ 7 ] );
            process.exit();
        }
        let forceUnique = getUniqueifierFunction();
        let duplicateAliasNotices = [];
        let hasAdmin = false;
        for ( let ob of users ) {
            // Validate all expected keys present...
            if ( !ob.icon || !ob.name || !ob.alias || !ob.pin || typeof ob.admin == undefined || !ob.dailyMinPoints || typeof ob.deleted == undefined ) {
                console.log( ERRORS[ 4 ] );
                process.exit();
            }
            if ( ob.admin ) {
                hasAdmin = true;
            }
            let originalAlias = ob.alias;
            ob.alias = forceUnique( ob.alias );
            if ( originalAlias != ob.alias ) {
                duplicateAliasNotices.push( ` * Found '${ originalAlias }' at least twice. Changed this one to '${ ob.alias }'.` );
            }
            this[ ob.alias ] = ob; // Add it into the class as an instance member.
        }
        if ( !hasAdmin ) {
            console.log( ERRORS[ 8 ] );
            process.exit();
        }
        if ( duplicateAliasNotices.length > 0 ) {
            console.log( NOTES[ 4 ] );
            duplicateAliasNotices.forEach( ( notice ) => { console.log( notice ); } );
        }
    }
    *[Symbol.iterator]() {
        for ( let user of Object.keys( this ) ) {
            yield this[ user ];
        }
    }
    toJSON() {
        let serialized = [];
        for ( let user of this ) {
            serialized.push( user );
        }
        return serialized;
    }
    userAliasToName( userAlias ) {
        let user = this[ userAlias ];
        return ( user ? user.name : "Not Found" );
    }
}

class GroupManager {
    constructor() {
        if ( !fs.existsSync( FILES[ "groups" ] ) ) {
            let defaultGroups = [ { // Sample
                alias: "adult", // Alias must be unique
                icon: "defaultgroup.png",
                nameSingular: "adult",
                namePlural: "adults",
                users: [ "someuseralias" ] // list of user alias that belong to this group
                // __Generated additional data__
                // inactive: bool -- if the groups has no users
            } ];
            fs.writeFileSync( FILES[ "groups-sample" ], JSON.stringify( defaultGroups, null, 4 ), { encoding: "utf8" } );
            fs.writeFileSync( FILES[ "groups" ], JSON.stringify( [], null, 4 ), { encoding: "utf8" } );
            console.log( NOTES[ 1 ] );
        }
        try { 
            var groups = JSON.parse( fs.readFileSync( FILES[ "groups" ], { encoding: "utf8" } ) );
        }
        catch ( ex ) {
            console.log( ERRORS[ 1 ] );
            process.exit();
        }
        // Ensure unique
        let forceUnique = getUniqueifierFunction();
        // Seed it with the To-be-created "all" group, so that no other group will have this alias.
        forceUnique( "all" );
        let allGroup = this[ "all" ] = {
            alias: "all",
            icon: "defaultgroup.org",
            nameSingular: "anyone",
            namePlural: "everyone",
            users: [],
            inactive: false
        };
        let duplicateAliasNotices = [];
        for ( let ob of groups ) {
            // Validate all expected keys present...
            if ( !ob.alias || !ob.icon || !ob.nameSingular || !ob.namePlural || !ob.users || !Array.isArray( ob.users ) ) {
                console.log( ERRORS[ 5 ] );
                process.exit();
            }
            let originalAlias = ob.alias;
            ob.alias = forceUnique( ob.alias );
            if ( originalAlias != ob.alias ) {
                duplicateAliasNotices.push( ` * Found '${ originalAlias }' at least twice (includes built-in names). Changed this one to '${ ob.alias }'.` );
            }
            // Extend the name value
            ob.nameSingular = `one ${ ob.nameSingular }`;
            ob.namePlural = `all ${ ob.namePlural }`;
            // Check that the user list is valid, has no dups, and is not empty.
            let userDeDupMap = {};
            for ( let user of ob.users ) {
                // No dups
                if ( userDeDupMap[ user ] ) {
                    console.log( ` * group '${ ob.alias }' named a user '${ user }' that is already in its list. Please remove duplicates.` );
                    continue;
                }
                // Ensure that the user exists
                if ( !userMgr[ user ] ) {
                    console.log( ` * group '${ ob.alias }' named a user '${ user }' that can't be found. This entry will not be added to the group.` );
                    continue;
                }
                userDeDupMap[ user ] = 1;
            }
            // Add the de-duped and valdated list into the object (which could be an empty list)
            ob.users = Object.keys( userDeDupMap );
            // Add the extra keys:
            ob.inactive = ( ob.users.length == 0 ); // If the list is empty, then this group is inactive.
            if ( ob.inactive ) {
                console.log( `Server start-up: NOTE, group "${ ob.alias }" has an empty user list in '${ FILES[ "users" ] }' and is marked inactive (it will not be available).` );
            }
            // Add in the group to this object
            this[ ob.alias ] = ob;
        }
        if ( duplicateAliasNotices.length > 0 ) {
            console.log( NOTES[ 5 ] );
            duplicateAliasNotices.forEach( ( notice ) => { console.log( notice ); } );
        }
        // Add default user groups (one for each user)
        for ( let userOb of userMgr ) { 
            allGroup.users.push( userOb.alias );
            // Synthesize a new user group
            let nonConflictingUserAlias = forceUnique( userOb.alias );
            this[ nonConflictingUserAlias ] = {
                alias: nonConflictingUserAlias,
                icon: "defaultgroup.org",
                // nameSingular: userOb.name, --this is the indicator not to create a "one" external group for this entry.
                namePlural: userOb.name,
                users: [ userOb.alias ],
                inactive: false
            };
        }
    }
    getDefaultGroup() { return "oneall" }
    has( choreGroupId ) {
        let rawGroup = this[ choreGroupId.substring( 3 ) ]; // break off the prefix and use the remainer to index...
        return ( rawGroup ? !rawGroup.inactive : false ); // If inactive, then act as if this group doesn't exist.
    }
    // Returns user list, or empty list if no users!
    getUserAliasesForGroup( choreGroupId ) {
        let groupOb = this[ choreGroupId.substring( 3 ) ];
        if ( !groupOb || groupOb.inactive ) {
            return [];
        }
        return groupOb.users;
    }
    getGroupMapSummary() {
        let map = {};
        // Create external-facing IDs
        for ( let groupName of Object.keys( this ) ) {
            let groupOb = this[ groupName ];
            if ( !groupOb.inactive ) {
                map[ `all${ groupName }` ] = { name: groupOb.namePlural };
                if ( groupOb.nameSingular ) {
                    map[ `one${ groupName }` ] = { name: groupOb.nameSingular };    
                }
            }
        }
        return map;
    }
    // True if only one user is allowed to have a selection at a time, e.g., "one" vs. "all"
    isOnePerGroup( choreGroupId ) {
        return ( choreGroupId.substring( 0, 3 ) == "one" );
    }
    toJSON() {
        // Turn the dictionary-based data store into a list for serialization.
        let serialized = [];
        for ( let key of Object.keys( this ) ) {
            let group = this[ key ];
            if ( key != "all" && group.nameSingular ) { // 'all' and missing nameSingular entries are dynamically generated
                serialized.push( {
                    alias: group.alias,
                    icon: group.icon,
                    nameSingular: group.nameSingular, // If undefined, it will not be serialized by JSON :-)
                    namePlural: group.namePlural,
                    users: group.users
                    // omits inactive
                } );
            }
        }
        return serialized;
    }
}

// Reads in the saved chore list, or a CSV, or creates a default chore list
class ChoreManager {
    constructor() {
        let hasChoresJSON = fs.existsSync( FILES[ "chores" ] );
        let hasChoresCSV = fs.existsSync( FILES[ "csv" ] );
        if ( !hasChoresJSON && !hasChoresCSV ) { // Create the defaults...
            let defaultChores = [ { // Implicit index (in this array is used as id)
                icon: "defaultchore.png",
                alias: "mkdinner",
                name: "make dinner",
                points: 2,
                deleted: false
            }, {
                icon: "defaultgroup.png",
                alias: "roomcln",
                name: "clean your room",
                points: 1,
                deleted: false
            } ];
            fs.writeFileSync( FILES[ "chores" ], JSON.stringify( defaultChores, null, 4 ), { encoding: "utf8" } );
            console.log( NOTES[ 2 ] );
        }
        else if ( !hasChoresJSON ) { // Either a JSON, or a CSV (or both) exists...
            // Try to read the CSV...
            let csvString = fs.readFileSync( FILES[ "csv" ], { encoding: "utf8" } );
            let [ choresList, errors ] = importCSV( csvString );
            if ( errors.length > 0 ) {
                console.log( ERRORS[ 2 ] );
                errors.forEach( ( errorString ) => { console.log( " * " + errorString ); } );
                process.exit();
            }
            // success! Supplement chore data structure
            let forceUnique = getUniqueifierFunction();
            choresList.forEach( ( choreOb ) => {
                choreOb.icon = "defaultchore.png";
                choreOb.alias = forceUnique( generateAlias( choreOb.name ) );
                choreOb.deleted = false;
                // name and points should already be present from the import.
            } );
            fs.writeFileSync( FILES[ "chores" ], JSON.stringify( choresList, null, 4 ), { encoding: "utf8" } );
            console.log( NOTES[ 3 ] );
        }
        try { 
            var chores = JSON.parse( fs.readFileSync( FILES[ "chores" ], { encoding: "utf8" } ) );
        }
        catch ( ex ) {
            console.log( ERRORS[ 3 ] );
            process.exit();
        }
        // Some basic chores data structure validation...
        let forceUnique = getUniqueifierFunction();
        let duplicateAliasNotices = [];
        for ( let ob of chores ) {
            // Validate all expected keys present...
            if ( !ob.alias || !ob.icon || !ob.name || typeof ob.points == undefined || typeof ob.deleted == undefined ) {
                console.log( ERRORS[ 6 ] );
                process.exit();
            }
            let originalAlias = ob.alias;
            ob.alias = forceUnique( ob.alias );
            if ( originalAlias != ob.alias ) {
                duplicateAliasNotices.push( ` * Found '${ originalAlias }' at least twice. Changed this one to '${ ob.alias }'.` );
            }
            // Add the chore object to the class
            this[ ob.alias ] = ob;
        }
        if ( duplicateAliasNotices.length > 0 ) {
            console.log( NOTES[ 6 ] );
            duplicateAliasNotices.forEach( ( notice ) => { console.log( notice ); } );
        }
    }
    *[Symbol.iterator]() {
        for ( let ob of Object.keys( this ) ) {
            yield this[ ob ];
        }
    }
    toJSON() {
        let list = [];
        for ( let name of Object.keys( this ) ) {
            list.push( this[ name ] );
        }
        return list;
    }
}

// Takes a file string (assumed to be CSV), and returns an [result, error list] (where error list may be 0-length list for success)
function importCSV( csvEntireFileAsString ) {
    let csvErrorList = [];
    let choreList = [];
    try {
        choreList = csvSyncParser( csvEntireFileAsString, { 
            columns: [ "name", "points" ],
            cast: function csvColumnConverter( val, context ) {
                if ( context.column == "name" ) {
                    if ( val == "" ) {
                        csvErrorList.push( `CSV Import Error: Blank chore name (column 1) value in CSV file (not allowed), approx row ${ context.lines}!` );
                    }
                    return val;
                }
                else if ( context.column == "points" ) {
                    let number = parseInt( val );
                    if ( number < 0 || Number.isNaN( number ) ) {
                        csvErrorList.push( `CSV Import Error: points (column 2) value is less-than 0 (out of range values) on approx. row ${ context.lines }!` );
                    }
                    return number;
                }
            },
            trim: true 
        } );
    }
    catch ( ex ) {
        csvErrorList.push( ex );
    }
    // choreList is [ { name: "", points: "" } ]
    return [ choreList, csvErrorList ];
}

function generateAlias( choreString ) {
    let splitBySpace = choreString.split( " " );
    if ( splitBySpace.length == 1 ) { // There were no spaces
        return splitBySpace[ 0 ].substring( 0, 3 ); // Up-to the first 3 characters
    }
    else if ( splitBySpace.length < 4 ) { // Up to 3 words
        return splitBySpace.reduce( ( prevVal, value ) => {
            return prevVal + value.substring( 0, 2 ); // first 2 characters of each word.
        }, "" );
    }
    else {
        return splitBySpace.reduce( ( prevVal, value, index ) => {
            if ( index < 10 ) { // Up to 9 words
                return prevVal + value.substring( 0, 1 ); // first character of each word
            }
            else {
                return prevVal; // Skip everything after 9 words.
            }
        }, "" );
    }
}

// Returns a function for ensuring the value provided is unique.
function getUniqueifierFunction() {
    let uniqueSet = new Set();
    // Returns a function that ensures the provided value is unique by 
    // appeding a 1,2,3... etc. to the end if a duplicate is found, and returning that.
    return ( maybeNotUnique ) => {
        let ordinalPostfix = 1;
        let originalNotUnique = maybeNotUnique;
        while ( uniqueSet.has( maybeNotUnique ) ) {
            maybeNotUnique = `${ originalNotUnique }${ ordinalPostfix++ }`; 
        }
        uniqueSet.add( maybeNotUnique );
        return maybeNotUnique; // Except it's not unique :-)
    }
}

// Day codes:
// 0 = unavailable
// 1 = available
// 2 = selected

// This doesn't store any state...
class AvailabilityManager {
    constructor() {
        //   CONSECUTIVE DAYS
        this[ "7xDaily" ]   = { name: "daily (Sun - Sat)",                  refresh: "1111111111111111111111111111", weekly: { min: 7, max: 7 } };
        this[ "5xWkDay" ]   = { name: "every weekday (Mon - Fri)",          refresh: "0111110011111001111100111110", weekly: { min: 5, max: 5 } };
        this[ "5xSklNite" ] = { name: "every school night (Sun - Thur)",    refresh: "1111100111110011111001111100", weekly: { min: 5, max: 5 } };
        this[ "2xWkEnd" ]   = { name: "all weekend (Sat - Sun)",            refresh: "1000001100000110000011000001", weekly: { min: 2, max: 2 } };
        this[ "evenodd"]    = { name: "every other day",                    refresh: "1111111111111111111111111111", weekly: { min: 3, max: 4 }, noAdjacency: 1 };
        //   SOMEWHAT WEEKLY
        this[ "3xWk" ]      = { name: "3 times per week (spaced apart)",    refresh: "1111111111111111111111111111", weekly: { min: 3, max: 3 }, noAdjacency: 1 };
        this[ "2xWk" ]      = { name: "2 times per week (spaced apart)",    refresh: "1111111111111111111111111111", weekly: { min: 2, max: 2 }, noAdjacency: 2 };
        this[ "1xSklNite" ] = { name: "once on a school night (Sun - Thur)",refresh: "1111100111110011111001111100", weekly: { min: 1, max: 1 } };
        this[ "1xWkDay" ]   = { name: "once on a weekday (Mon - Fri)",      refresh: "0111110011111001111100111110", weekly: { min: 1, max: 1 } };
        this[ "1xWkEnd" ]   = { name: "once on the weekend (Sat or Sun)",   refresh: "1000001100000110000011000001", weekly: { min: 1, max: 1 } };
        this[ "1xWk" ]      = { name: "once weekly (Sun - Sat)",            refresh: "1111111111111111111111111111", weekly: { min: 1, max: 1 } };
        this[ "1x>=Wed"]    = { name: "once weekly (Wed or later)",         refresh: "0001111000111100011110001111", weekly: { min: 1, max: 1 } };
        //   LESS FREQUENTLY
        this[ "3xMonth" ]   = { name: "3 times per month (spaced apart)",   refresh: "1111111111111111111111111111", monthly: { min: 3, max: 3 }, noAdjacency: 8 };
        this[ "2xMonth" ]   = { name: "2 times per month (spaced apart)",   refresh: "1111111111111111111111111111", monthly: { min: 2, max: 2 }, noAdjacency: 10 };
        this[ "1xSkipWk" ]  = { name: "once in two weeks",                  refresh: "1111111111111111111111111111", monthly: { min: 2, max: 2 }, noAdjacency: 14 };
        this[ "1xMonth" ]   = { name: "monthly",                            refresh: "1111111111111111111111111111", monthly: { min: 1, max: 1 } };
        //   CUSTOM
        
        // Convert 'refresh' strings into arrays
        for ( let availAlias of Object.keys( this ) ) {
            let array = [];
            for ( let i = 0, len = this[ availAlias ].refresh.length; i < len; i++ ) {
                if ( this[ availAlias ].refresh[ i ] == "1" ) {
                    array.push( 1 );
                }
                else {
                    array.push( 0 );
                }
            }
            this[ availAlias ].refresh = array;
        }
    }
    // Returns null if successfully added, an error string otherwise.
    addCustomAvailability( declarativeDescriptor ) {
        // {
        //    alias: "2:045", // Alias must be unique (e.g., is generated: 2 times a week, available only on Sunday, Thursday, and Friday)
        //    name: ""
        //    scope: "week" | "month"
        //    min: 2,
        //    max: 2
        //    days: [ 0, 4, 5 ],
        //    selectionGap: 2, // optional
        // }
        // VALIDATE
        if ( typeof declarativeDescriptor.name != "string" || typeof declarativeDescriptor.alias != "string" || declarativeDescriptor.name == "" || declarativeDescriptor.alias == "" ) {
            return "The name and alias properties must be strings and not empty."
        }
        // Gather up existing aliases, to ensure this one is unique...
        let makeUnique = getUniqueifierFunction();
        for ( let existingAlias of Object.keys( this ) ) {
            makeUnique( existingAlias );
        }
        let possibleNewAlias = makeUnique( declarativeDescriptor.alias );
        if ( possibleNewAlias != declarativeDescriptor.alias ) {
            return `The alias '${ declarativeDescriptor.alias }' is a duplicate of another pre-existing alias, please ensure that it is unique.`;
        }
        // Ensure scope is legit.
        if ( declarativeDescriptor.scope != "week" && declarativeDescriptor.scope != "month" ) {
            return "The scope value must be either 'week' or 'month'.";
        }
        if ( typeof declarativeDescriptor.min != "number" || typeof declarativeDescriptor.max != "number" || declarativeDescriptor.min < 0 || declarativeDescriptor.max < 0 ) {
            return "The min and max values must be postive numbers.";
        }
        if ( declarativeDescriptor.min > declarativeDescriptor.max ) {
            return "The min value must be less-than or equal to the max value.";
        }
        if ( !Array.isArray( declarativeDescriptor.days ) ) {
            return "The days value must be an array of 0-based day integers.";
        }
        for ( let day of declarativeDescriptor.days ) {
            if ( typeof day != "number" || day < 0 || day >= ( ( declarativeDescriptor.scope == "week" ) ? 7 : 28 ) ) {
                return `The days array must only contain integers between 0 (inclusive) and ${ ( declarativeDescriptor.scope == "week" ) ? 7 : 28 } (exclusiv).`;
            }
        }
        if ( declarativeDescriptor.selectionGap ) {
            if ( typeof declarativeDescriptor.selectionGap != "number" || declarativeDescriptor.selectionGap < 1 ) {
                return "The selectionGap property is optional. If provided, it must be a positive integer that is 1 or greater.";
            }
        }
        // Add it to the object if it passed the gauntlet.
        let customDescriptor = { name: declarativeDescriptor.name };
        let dayCodes = this.getDefaultDayCodes();
        if ( declarativeDescriptor.scope == "month" ) {
            for ( let code of declarativeDescriptor.days ) {
                dayCodes[ code ] = 0;
            }
            customDescriptor.monthly = { 
                min: declarativeDescriptor.min,
                max: declarativeDescriptor.max
            };
        }
        else { // week-based codes
            for ( let code of declarativeDescriptor.days ) {
                // Replcate week-based codes across the entire 4-week month.
                for ( let week = 0; week < 4; week++ ) {
                    dayCodes[ ( week * 7 ) + code ] = 0;
                }
            }
            customDescriptor.weekly = { 
                min: declarativeDescriptor.min,
                max: declarativeDescriptor.max
            };
        }
        // Invert the codes ( 0 => 1, 1 => 0 )
        customDescriptor.refresh = dayCodes.map( ( code ) => ( code ? 0 : 1 ) );
        if ( declarativeDescriptor.selectionGap ) {
            customDescriptor.noAdjacency = declarativeDescriptor.selectionGap;
        }
        this[ declarativeDescriptor.alias ] = customDescriptor;
    }
    getDefaultAvailableId() { return "1xWk"; }
    getDefaultDayCodes() { 
        //     |   week 1    |   week 2    |   week 3    |   week 4    |
        return [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]; 
    }
    getAvailabilityMapSummary() {
        let map = {};
        for ( let mapKey of Object.keys( this ) ) {
            map[ mapKey ] = { name: this[ mapKey ].name };
        }
        return map;
    }
    refreshAvailability( availId, userId, userDays, isDependent ) {
        // 'refresh' is a string code for how to refresh the availability/unavailability codes
        // Does not necessarly imply conformance to the specific criteria.
        // If a targetUser is dependent (e.g., in a "one" group), then the refresh can only happen
        // if none of it's corresponding day dependent users have selected that day.
        let refreshCode = this[ availId ].refresh;
        let dayCodes = userDays[ userId ];
        if ( !dayCodes ) {
            return; // Some users may not have this chore on this day--don't require a refresh.
        }
        for ( let i = 0, len = refreshCode.length; i < len; i++ ) {
            if ( refreshCode[ i ] != dayCodes[ i ] && dayCodes[ i ] != 2 ) {
                if ( isDependent ) {
                    let hasOtherSelection = false;
                    for ( let otherUserId of Object.keys( userDays ) ) {
                        if ( otherUserId != userId && userDays[ otherUserId ][ i ] == 2 ) {
                            hasOtherSelection = true;
                            break;
                        }
                    }
                    if ( !hasOtherSelection ) {
                        dayCodes[ i ] = refreshCode[ i ];
                    }
                }
                else {
                    dayCodes[ i ] = refreshCode[ i ]; // Assumes the zero or one as specified.
                }
            }
        }
    }
    checkInvalidSelections( availId, users, userAlias, isOnePerGroup, dayRange, errorObject ) {
        let invalidSelection = [];
        // Basic check based on broad refresh codes...
        let code = this[ availId ].refresh;
        for ( let [ start, end ] = dayRange; start < end; start++ ) {
            if ( users[ userAlias] && users[ userAlias ][ start ] == 2 && code[ start ] == 0 ) {
                invalidSelection.push( start );
            }
        }
        if ( invalidSelection.length > 0 ) {
            errorObject.errorReason = `Remove selection from ${ invalidSelection.map( ( index ) => monthIndexToDayNameAndWeek( index ) ).join(", ") } which are not valid days to do this chore.`;
            return true;
        }
        // Execute specific invalid logic if defined
        if ( this[ availId ].noAdjacency && users[ userAlias ] ) { // Because this user may not have to worry about it.
            // Note: treat 'invalidSelection' elements as an error string collector only
            // validate no adjacency...
            let [ start, end ] = dayRange;
            let adjacencyMin = this[ availId ].noAdjacency;
            let lastSelected = -1; // -1 for none (start state).
            let lastWho = null;
            let otherUsers = Object.keys( users ).filter( name => name != userAlias );
            // Review the entire month range...
            for ( let i = 0, len = users[ userAlias ].length; i < len; i++ ) {
                let who = userAlias;
                let isDaySelected = users[ userAlias ][ i ] == 2;
                // Compute logical OR of all users that have this chore, but only if the selected user doesn't already compute to selected
                if ( isOnePerGroup && !isDaySelected ) {
                    for ( let otherUser of otherUsers ) {
                        isDaySelected = users[ otherUser ][ i ] == 2;
                        if ( isDaySelected ) {
                            who = otherUser;
                            break;
                        }
                    }
                }
                // State 0: find the first selection
                if ( lastSelected == -1 && isDaySelected ) {
                    lastSelected = i;
                    lastWho = who;
                }
                // State 2: selection found!
                else if ( isDaySelected ) {
                    // if minimum not met (note <=, since adjacent selections have a difference of 1 (not zero)) AND this in in the week requested...
                    if ( ( ( i - lastSelected ) <= adjacencyMin ) && 
                        ( ( lastSelected >= start && lastSelected < end && lastWho == userAlias ) || 
                        ( i >= start && i < end && who == userAlias ) ) ) { 
                        invalidSelection.push( `${ monthIndexToDayNameAndWeek( lastSelected, userAlias, lastWho ) } and ${ monthIndexToDayNameAndWeek( i, userAlias, who ) }` );
                    }
                    lastSelected = i;
                    lastWho = who;
                }
            }
            if ( invalidSelection.length > 0 ) {                
                errorObject.errorReason = `The selections between ${ invalidSelection.join(", ") } must be at least ${ adjacencyMin } day${ ( adjacencyMin > 1 ) ? "s" : "" } apart.`;
                return true;
            }
        }
        return false;
    }
    // Checks for completeness and adds incompleteReason if applicable (may also add unavailableReason and 
    // set available state if conditions are right)
    meetsRequirements( availId, users, userAlias, isOnePerGroup, dayRange, completeObject ) {
        let otherUsers = Object.keys( users ).filter( name => name != userAlias );
        let hasAtLeastOneSelectionByUserAlias = false;
        // Assume monthly...
        let { min, max } = ( this[ availId ].weekly ? this[ availId ].weekly : this[ availId ].monthly );
        let start = 0;
        let end = 4 * 7;
        if ( this[ availId ].weekly ) { // but if Weekly Reqs...
            [ start, end ] = dayRange;
        }
        let sum = 0;
        for ( ; start < end; start++ ) {
            let isSelected = ( users[ userAlias] && users[ userAlias ][ start ] == 2 );
            if ( isSelected ) {
                hasAtLeastOneSelectionByUserAlias = true;
            }
            if ( isOnePerGroup && !isSelected ) {
                for ( let otherUser of otherUsers ) {
                    if ( users[ otherUser ][ start ] == 2 ) {
                        isSelected = true
                        break;
                    }
                }
            }
            if ( isSelected ) {
                sum++;
            }
        }
        if ( sum < min ) {
            completeObject.incompleteReason = `${ ( min != max ) ? `At a minimum, t` : `T` }his chore must be done ${ this[ availId ].name }${ ( this[ availId ].monthly ?  ` (plan ahead and select this chore this week to get it done early).` : "." ) }`;
            return false;
        }
        else if ( sum > max ) {
            completeObject.incompleteReason = `This chore can only be selected ${ this[ availId ].name }.${ ( this[ availId ].monthly ? ` You have selected it too many times this week or in another week.` : "" ) }`;
            return false;
        }
        else if ( sum == max && !hasAtLeastOneSelectionByUserAlias ) { // Then, while this chore still meets the requirements, it's no longer available.
            completeObject.unavailableReason = `All available chore days (max of ${ max }) claimed by others.`;
            completeObject.available = false;
        }
        return true;
    }
    hasSelectedChoreAtAll( dayCodes ) {
        for ( let day of dayCodes ) {
            if ( day == 2 ) {
                return true;
            }
        }
        return false;
    }
    hasSelectedChoreInWeek( dayCodes, dayRange ) {
        for ( let [ start, end ] = dayRange; start < end; start++ ) {
            if ( dayCodes[ start ] == 2 ) {
                return true;
            }
        }
        return false;
    }
}

function loadOptionalCustomExtension() {
    if ( !fs.existsSync( FILES[ "custom" ] ) ) {
        let defaultCustomAvilability = {
            availabilities: [ {
                alias: "2:045",          // Alias must be unique (e.g., is generated: 2 times a week, available only on Sunday, Thursday, and Friday)
                name: "3 times a month", // readable name should follow similar pattern as other availabilities
                scope: "week|month",     // whether days should be limited to week or month index scope.
                days: [ 0, 4, 5 ],       // 0-based day offsets in the week or month
                selectionGap: 2,         // (optional) amount of gap required between selected days.
                min: 2,                  // minimum required days to fullfill the chore's requirements
                max: 2,                  // maximum number of days allowed for this chore in the scope's time period.
            } ]
            // This can be [possibly] extended in the future.
        }; 
        fs.writeFileSync( FILES[ "custom-sample" ], JSON.stringify( defaultCustomAvilability, null, 4 ), { encoding: "utf8" } );
        fs.writeFileSync( FILES[ "custom" ], JSON.stringify( { availabilities: [] }, null, 4 ), { encoding: "utf8" } );
        console.log( NOTES[ 7 ] );
    }
    try {
        var custom = JSON.parse( fs.readFileSync( FILES[ "custom" ], { encoding: "utf8" } ) );
    }
    catch ( ex ) {
        console.log( ERRORS[ 9 ] );
        process.exit();
    }
    // Ensure top-level keys present and are arrays...
    if ( !custom.availabilities || !Array.isArray( custom.availabilities ) ) {
        console.log( ERRORS[ 10 ] );
        process.exit();
    }
    // Availability object validation is handled by the AvailabilityManager
    for ( let i = 0, len = custom.availabilities.length; i < len; i++ ) {
        if ( typeof custom.availabilities[ i ] != "object" || custom.availabilities[ i ] == null ) {
            console.log( ERRORS[ 11 ] );
            process.exit();
        }
        let message = availMgr.addCustomAvailability( custom.availabilities[ i ] );
        if ( message ) {
            console.log( `Server start-up error: loading configuration file '${ FILES[ "custom" ] }' failed. ${ message }` );
            process.exit();
        }
    }
}

// Loads the saved state which has the relationship between chores <-> availability <-> groups
// Requires that userMgr, groupMgr, choreMgr are already loaded.
class StateManager {
    constructor() {
        if ( !fs.existsSync( FILES[ "state" ] ) ) {
            // Construct the initial state based on existing loaded data...
            // submits: {
            //     "username": [0,0,0,0] // 0 - draft, 1 - submitted, 2 - approved (by week 0-3)
            // }
            // choreMap: { "choreAlias": {
            //     groupId: "groupAlias" // group assigned to this chore...
            //     availabilityId: "availabilityAlias" // availability assigned to this chore
            //     userData: {
            //        "username": [1,1,1,1,...,1], // Always check that the user is still available
            //        "usrname2": [1,1,1,1,...,1]  // Slice this into weeks for transport.
            //                     ( 0 = unavailable, 1 = available, 2 = selected )
            //     }
            // } }
            let defaultState = { submits: {}, choreMap: {} };
            for ( let user of userMgr ) {
                defaultState.submits[ user.alias ] = [0,0,0,0]; // Drafts...all weeks.
            }
            for ( let chore of choreMgr ) {
                if ( !chore.deleted ) {
                    defaultState.choreMap[ chore.alias ] = StateManager.createDefaultChoreState();
                }
            }
            fs.writeFileSync( FILES[ "state" ], JSON.stringify( defaultState ), { encoding: "utf8" } );
            console.log( NOTES[ 8 ] );
        }
        try {
            var state = JSON.parse( fs.readFileSync( FILES[ "state" ], { encoding: "utf8" } ) );
        }
        catch ( ex ) {
            console.log( ERRORS[ 12 ] );
            process.exit();
        }
        // Ensure chore state is synced to the existing choreManager (e.g., deletes/adds have 
        // not happened between last persisted state map).
        for ( let choreAlias of Object.keys( state.choreMap ) ) {
            if ( !choreMgr[ choreAlias ] ) {
                delete state.choreMap[ choreAlias ]; // Stale entry from disk!
            }
        }
        // Cross-check that new chore objects haven't been newly-added...
        for ( let choreOb of choreMgr ) {
            if ( !choreOb.deleted && !state.choreMap[ choreOb.alias ] ) {
                state.choreMap[ choreOb.alias ] = StateManager.createDefaultChoreState();
            }
        }
        // Sync submit state with any added users (deleted users are left in)
        for ( let userAlias of Object.keys( userMgr ) ) {
            if ( !state.submits[ userAlias ] ) { // New user...
                state.submits[ userAlias ] = [0,0,0,0]; // Default (drafts)
                // Add this user to all the chores in the chore map.
                for ( let choreAlias of Object.keys( state.choreMap ) ) {
                    state.choreMap[ choreAlias ].userData[ userAlias ] = availMgr.getDefaultDayCodes();
                }
            }
        }
        this.submits = state.submits;
        this.choreMap = state.choreMap;
    }
    static createDefaultChoreState() {
        let choreState = {
            groupId: groupMgr.getDefaultGroup(),
            availabilityId: availMgr.getDefaultAvailableId()
        };
        let userState = {};
        for ( let applicableUser of groupMgr.getUserAliasesForGroup( choreState.groupId ) ) {
            userState[ applicableUser ] = availMgr.getDefaultDayCodes();
        }
        choreState.userData = userState;
        return choreState;
    }
    // toJSON() not needed, as this object serialized properly.
    // ,
    getSubmitMap() {
        return this.submits;
    }
    getSubmitValues( userAlias ) {
        return this.submits[ userAlias ];
    }
    // Return true if the value was changed.
    // userAlias and weekIndex were pre-validated.
    setSubmitValue( userAlias, weekIndex, newValue ) {
        if ( typeof newValue != "number" || newValue < 0 || newValue > 2 ) {
            return false; // new state value is bad
        }
        this.submits[ userAlias ][ weekIndex ] = newValue;
        return true;
    }
    getChoreMapSimple() {
        let map = {};
        for ( let mapKey of Object.keys( this.choreMap ) ) {
            map[ mapKey ] = { groupId: this.choreMap[ mapKey ].groupId, availId: this.choreMap[ mapKey ].availabilityId };
        }
        return map;
    }
    setChoreMap( choreMapChange ) {
        if ( !this.validateChoreMapChangeRequest( choreMapChange ) ) {
            return false;
        }
        let stateOb = this.choreMap[ choreMapChange.chore ];
        let confirmObject = { [ choreMapChange.chore ]: {} };
        let { availId, groupId } = choreMapChange.changeto;
        if ( typeof availId != "undefined" ) { // Avail ID change request
            stateOb.availabilityId = ( availId == null ? availMgr.getDefaultAvailableId() : availId );
            confirmObject[ choreMapChange.chore ].availId = stateOb.availabilityId;
        }
        else { // Group ID change request
            stateOb.groupId = ( groupId == null ? groupMgr.getDefaultGroup() : groupId );
            confirmObject[ choreMapChange.chore ].groupId = stateOb.groupId;
        }
        this.saveState();
        return confirmObject;
    }
    validateChoreMapChangeRequest( ob ) {
        // { chore: 'xxx', changeto: { availId|groupId: 'xxxx'|null } }
        if ( typeof ob != "object" || ob == null ) {
            return false;
        }
        if ( typeof ob.chore != "string" || typeof ob.changeto != "object" || ob.changeto == null ) {
            return false;
        }
        if ( typeof ob.changeto.availId != "string" && ob.changeto.availId != null &&
            typeof ob.changeto.groupId != "string" && ob.changeto.groupId != null ) {
            return false;
        }
        if ( !this.choreMap[ ob.chore ] ) {
            return false; // This chore does not exist
        }
        if ( ( ob.changeto.availId && !availMgr[ ob.changeto.availId ] ) ||
            ( ob.changeto.groupId && !groupMgr.has( ob.changeto.groupId ) ) ) {
            return false;
        }
        return true;
    }
    // Week index is 0-3
    getReportList( weekIndex, userAlias ) {
        let states = [];
        for ( let choreAlias of Object.keys( this.choreMap ) ) {
            states.push( this.getReport( choreAlias, this.choreMap[ choreAlias ], weekIndex, userAlias ) );
        }
        return states;
    }
    // Returns just the states for which the user has selected something
    getReportListSelected( weekIndex, userAlias ) {
        let states = [];
        for ( let choreAlias of Object.keys( this.choreMap ) ) {
            let state = this.getReport( choreAlias, this.choreMap[ choreAlias ], weekIndex, userAlias );
            if ( state.week.includes( "2" ) ) {
                states.push( state );
            } 
        }
        return states;
    }
    getReport( choreAlias, stateOb, weekIndex, userAlias ) {
        // {
        //    alias (string)              -- which chore this is.
        //  ? errorReason (string)        -- Note on structural issues that must be addressed before submission.
        //    available (bool)            -- does this chore have any relevant actions for this user? (show these by default)
        //  ? unavailableReason (string)  -- reason for a 'false' value above.
        //    complete (bool)             -- are the requirements for this chore satisfied?
        //  ? incompleteReason (string)   -- reason for a 'false' value above.
        //    week: "xxxxxxx" (string)    -- state codes for the various days of the requested week ( 0 = unavailable, 1 = available, 2 = selected )
        //  ? otherUserWeek: {
        //        user: "xxxxxxx"         -- state codes for other relevant users on this chore (for optional expansion in the UI)
        //    }
        // }
        let choreState = { alias: choreAlias };
        let dayRange = this.weekIndex2Range( weekIndex );
        let isOnePerGroup = groupMgr.isOnePerGroup( stateOb.groupId );
        // First: non-error-state ensure:
        // - swap available days to unavailable per rules, and unavailable to available per rules
        availMgr.refreshAvailability( stateOb.availabilityId, userAlias, stateOb.userData, isOnePerGroup );
        // Then, ensure chore (to check for error-state), set any error-state (errorReason) details.
        if ( this.isErrorState.call( choreState, stateOb, dayRange, userAlias ) ) {
            choreState.available = true;
        }
        else {
            // Get availability for chore + availability reason (unavailableReason)
            choreState.available = this.isAvailable.call( choreState, stateOb, dayRange, userAlias );
        }
        // Perform a "requried" check + add requirements notice (incompleteReason)
        // Note: as a result of the chore having its requirements met, this method can also set (unavailableReason)
        // and available=false when the current user didn't contribute any selections toward the chore's requirements,
        // even if there are still available days left for this user in this week (because any change to select 
        // even one more day for this chore would make it exceed the maximum chore requirements).
        choreState.complete = availMgr.meetsRequirements( stateOb.availabilityId, stateOb.userData, userAlias, isOnePerGroup, dayRange, choreState );
        // Get day range for user
        // If "one" then get additional chore state in day range for all other users that have at least one selected chore
        if ( !stateOb.userData[ userAlias ] ) {
            choreState.week = "0000000"; // All unavailable.
        }
        else {
            choreState.week = "";
            for ( let [ start, end ] = dayRange; start < end; start++ ) {
                choreState.week += String( stateOb.userData[ userAlias ][ start ] );
            }
        }
        if ( groupMgr.isOnePerGroup( stateOb.groupId ) ) {
            let groupUsers = groupMgr.getUserAliasesForGroup( stateOb.groupId );
            if ( groupUsers.length > 1 ) {
                let otherSelectedChores = {};
                for ( let userName of groupUsers ) {
                    if ( userName != userAlias ) {
                        // Only surface users who have at least one selected chore in the current week. 
                        let otherUserChores = "";
                        let hasSelectedChore = false;
                        for ( let [ start, end ] = dayRange; start < end; start++ ) {
                            if ( stateOb.userData[ userName ][ start ] == 2 ) {
                                hasSelectedChore = true;
                            }
                            otherUserChores += String( stateOb.userData[ userName ][ start ] );
                        }
                        if ( hasSelectedChore ) {
                            otherSelectedChores[ userName ] = otherUserChores;
                        }
                    }
                }
                if ( Object.keys( otherSelectedChores ).length > 0 ) {
                    choreState.otherUserWeek = otherSelectedChores; 
                }
            }
        }
        return choreState;
    }
    // 'this' is the choreState (which will be returned)
    isErrorState( stateOb, dayRange, userAlias ) {
        // If a chore is in error-state, it cannot be submitted for approval until the error is resolved.
        //  Note: error-state does not include not meeting being deficient for a chores's quota...
        // Triggers for error-state:
        // - check that the right users are present according to the group
        let assignedUsers = groupMgr.getUserAliasesForGroup( stateOb.groupId );
        let existingUsers = Object.keys( stateOb.userData );
        for ( let assignedUser of assignedUsers ) {
            if ( !existingUsers.includes( assignedUser ) ) {
                // Add this user -- not an errorReason (yet)
                stateOb.userData[ assignedUser ] = availMgr.getDefaultDayCodes();
            }
        }
        for ( let existingUser of existingUsers ) {
            if ( !assignedUsers.includes( existingUser ) && existingUser == userAlias ) { // Keep the message relevant to the selected user only
                if ( availMgr.hasSelectedChoreAtAll( stateOb.userData[ existingUser ] ) ) {
                    if ( availMgr.hasSelectedChoreInWeek( stateOb.userData[ existingUser ], dayRange ) ) {
                        this.errorReason = "Please remove the selected days from this chore (this chore is no longer available to you).";
                        return true;
                    }
                    // otherwise, it is ignored--will be addressed in another week (if investigated).
                }
                else {
                    // remove it, no selected chores in the month (no error condition needed)
                    delete stateOb.userData[ existingUser ];
                }
            }
        }
        existingUsers = Object.keys( stateOb.userData ); // As some may have been removed (see above)
        let isOnePerGroup = groupMgr.isOnePerGroup( stateOb.groupId );
        // - check that for "one", there are no two vertical users with the same chore selected
        if ( isOnePerGroup ) {
            // ExistingUsers may have changed, but changes won't impact this check. E.g., newly 
            // added users have default settings that don't include selections, and removed users
            // would only have been removed if they didn't have selections!
            for ( let [ start, end ] = dayRange; start < end; start++ ) {
                let selectedUsers = [];
                for ( let existingUser of existingUsers ) {
                    if ( stateOb.userData[ existingUser ][ start ] == 2 ) {
                        selectedUsers.push( existingUser );
                    }
                }
                if ( selectedUsers.length > 1 && selectedUsers.includes( userAlias ) ) {
                    // Problem (for this user)
                    this.errorReason = `${ selectedUsers.map( userMgr.userAliasToName.bind( userMgr ) ).join(", ") } have all selected this chore on the same day (only one of you is allowed to have it)`;
                    return true;
                }
            }
        }
        // available-specific check:
        //  - ensure no selected chores that are out-of-bounds for the availability, may set the errorReason as appropriate.
        return availMgr.checkInvalidSelections( stateOb.availabilityId, stateOb.userData, userAlias, isOnePerGroup, dayRange, this );
    }
    // Return chore available status (and set availableState if applicable)
    isAvailable( choreOb, dayRange, userAlias ) {
        // Otherwise (not error-state), if the requested user is not in the relevant user list, the chore is not available
        let users = groupMgr.getUserAliasesForGroup( choreOb.groupId );
        if ( !users.includes( userAlias ) ) {
            this.unavailableReason = `This chore does not apply to you.`;
            return false;
        }
        // If the requested user has any available day slots (or selected) (in the day range), then its available, otherwise not.
        for ( let [ start, end ] = dayRange; start < end; start++ ) {
            if ( choreOb.userData[ userAlias ][ start ] != 0 ) { // 1 or 2 (available or selected)
                return true;
            }
        }
        this.unavailableReason = `All days claimed by others, no more available days this week.`
        return false;
    }
    // Returns a success or error property on an object for each request along with explanation
    // (the data for the 'update' is overwritten by each successive same-chore request--it's always the 
    //  most recent with in-between values omitted)
    // success = requested value was set | error = requested value was not set
    // * { results: [ { success: true, update: "choreId" } ], updates: { "choreId": {...} } }
    // * { results: [ { error: 14, message: "e.g., precheck validation failure--chore not found" }, ... }
    selectChores( userAlias, selectionList ) {
        var finalResults = { updates: {} };
        finalResults.results = selectionList.map( ( selectOb ) => {
            let { chore:choreAlias, week:weekIndex, day:dayIndex, select } = selectOb;
            let resultsOb = {};
            if ( !this.validateSelectionRequest( resultsOb, choreAlias, userAlias, weekIndex, dayIndex, select ) ) {
                return resultsOb; // Filled out by the above function call.
            }
            let stateOb = this.choreMap[ choreAlias ];
            let monthIndex = ( weekIndex * 7 ) + dayIndex;
            let currentTargetValue = stateOb.userData[ userAlias ][ monthIndex ];
            // Each change in selection state [may] effect other validity/chore availability. 
            // Check for immediate availability of the request chore.
            if ( currentTargetValue == 0 ) {
                resultsOb.error = 20;
                resultsOb.message = "requested day unavailable";
            }
            else if ( ( select && currentTargetValue == 2 ) || ( !select && currentTargetValue == 1 ) ) {
                resultsOb.success = true; // If for some reason, the client is re-confirming a chore in the same state, then just roll with it but don't do any other work.
            }
            else {
                resultsOb.success = true;
                // Available/Selected and the action will change the internal value
                this.choreMap[ choreAlias ].userData[ userAlias ][ monthIndex ] = ( select ? 2 : 1 ); // set or unset!
                if ( groupMgr.isOnePerGroup( stateOb.groupId ) ) {
                    for ( let otherUserAlias of groupMgr.getUserAliasesForGroup( stateOb.groupId ) ) { // Never tweak existing selections! (Rule)
                        let existingDays = this.choreMap[ choreAlias ].userData[ otherUserAlias ];
                        if ( otherUserAlias != userAlias && select && existingDays[ monthIndex ] == 1 ) {
                            existingDays[ monthIndex ] = 0; // No longer available!
                        }
                        else if ( otherUserAlias != userAlias && !select && existingDays[ monthIndex ] == 0 ) {
                            existingDays[ monthIndex ] = 1; // Now available!
                        }
                    }
                }
                this.saveState();
            }
            // Get (re-validate) the current state for this chore and add it to the resultsOb as the update
            // to this selection.
            resultsOb.update = choreAlias;
            finalResults.updates[ choreAlias ] = this.getReport( choreAlias, stateOb, weekIndex, userAlias );
            return resultsOb;
        } );
        return finalResults;
    }
    validateSelectionRequest( resultsOb, choreAlias, userAlias, weekIndex, dayIndex, select ) {
        // Don't trust anything the client sends--validate everything!
        if ( typeof choreAlias != "string" || typeof userAlias != "string" ) {
            resultsOb.error = 10;
            resultsOb.message = "Malformed request: 'chore' must be a string";
            return false; // Wrong type!
        }
        if ( typeof weekIndex != "number"  || typeof dayIndex != "number" ) {
            resultsOb.error = 11;
            resultsOb.message = "Malformed request: 'week' must be a number";
            return false; // Wrong type!
        }
        if ( typeof select != "boolean" ) {
            resultsOb.error = 12;
            resultsOb.message = "Malformed request: 'select' must be a boolean";
            return false; // Wrong type!
        }
        if ( weekIndex < 0 || weekIndex >= 4 || dayIndex < 0 || dayIndex >= 7 ) {
            resultsOb.error = 13;
            resultsOb.message = "Precheck validation: 'week' must be 0,3 and 'day' must be 0,6";
            return false; // Weekindex/ dayIndex out of bounds.
        }
        if ( !this.choreMap[ choreAlias ] ) {
            resultsOb.error = 14;
            resultsOb.message = "Precheck validation: 'chore' value is not an existing chore identifier";
            return false; // Chore doesn't exist anymore!
        }
        let choreOb = this.choreMap[ choreAlias ];
        if ( !groupMgr.getUserAliasesForGroup( choreOb.groupId ).includes( userAlias ) ) {
            resultsOb.error = 15;
            resultsOb.message = "Precheck validation: current user is not allowed to select this chore";
            return false; // User isn't valid for this chore.
        }
        return true;
    }
    submitChores( userAlias, weekIndex ) {
        let returnOb;
        let errorMsg = this.hasSubmitError( userAlias, weekIndex );
        if ( !errorMsg ) {
            let previousState = this.submits[ userAlias ][ weekIndex ];
            this.setSubmitValue( userAlias, weekIndex, 1 ); // either 0 => 1 or 1 => 1
            returnOb = { success: `Chores ${ previousState == 1 ? "re-" : "" }submitted for this week!` };
        }
        else {
            returnOb = { error: errorMsg };
        }
        return returnOb;
    }
    hasSubmitError( userAlias, weekIndex ) {
        if ( this.submits[ userAlias ][ weekIndex ] == 2 ) { // Trying to submit in 'approved' state? Uh, no.
            return `This week has already been submitted!`;
        }
        let errorCount = 0;
        for ( let stateOb of this.getReportList( weekIndex, userAlias ) ) {
            if ( stateOb.errorReason ) {
                errorCount++;
            }
        }
        if ( errorCount == 0 ) {
            return null;
        }
        return `${ errorCount } chore${ errorCount == 1 ? " has" : "s have" } errors. Please correct these before submission.`;
    }
    getSelectedChoresForWeek( userAlias, weekIndex ) {
        // Get the report list to ensure that all chores are up-to-date (otherwise data not used)
        this.getReportList( weekIndex, userAlias );
        // output is [ (dayIndex) [ choreAlias, ... ] ]
        let days = [ [], [], [], [], [], [], [] ];
        // Scan all the chores and extract selected (2) days into the correct bucket.
        let dayRange = this.weekIndex2Range( weekIndex );
        for ( let choreAlias of Object.keys( this.choreMap ) ) {
            let allChoreSlots = this.choreMap[ choreAlias ].userData[ userAlias ];
            if ( allChoreSlots ) { // Not every chore state has this user as applicable user!
                for ( let [ start, end ] = dayRange; start < end; start++ ) {
                    if ( allChoreSlots[ start ] == 2 ) {
                        days[ start % 7 ].push( choreAlias ); // this chore
                    }
                }
            }
        }
        return days;
    }
    clearChoreWeek( userAlias, weekIndex ) {
        for ( let choreAlias of Object.keys( this.choreMap ) ) {
            let userSelectionArray = this.choreMap[ choreAlias ].userData[ userAlias ];
            if ( userSelectionArray ) {
                for ( let [ start, end ] = this.weekIndex2Range( weekIndex ); start < end; start++ ) {
                    if ( userSelectionArray[ start ] == 2 ) { // Selected?
                        userSelectionArray[ start ] = 1; // Available now    
                    }
                }
            }
        }
        this.submits[ userAlias ][ weekIndex ] = 0; // back to draft.
        return { success: true };
    }
    // Clear all the weeks for a single user.
    clearChoreWeekPerUser( userAlias ) {
        for ( let weekIndex of [ 0, 1, 2, 3 ] ) {
            this.clearChoreWeek( userAlias, weekIndex );
        }
        return { success: true };
    }
    // Clear all the users selections for a given week
    clearChoreWeekPerWeek( weekIndex ) {
        for ( let userAlias of Object.keys( userMgr ) ) {
            this.clearChoreWeek( userAlias, weekIndex );
        }
        return { success: true };
    }
    // Wipes out all chore selections for all users in all weeks.
    clearChoreWeekAll() {
        for ( let userAlias of Object.keys( userMgr ) ) {
            for ( let weekIndex of [ 0, 1, 2, 3 ] ) {
                this.clearChoreWeek( userAlias, weekIndex );
            }
        }
        return { success: true };
    }
    weekIndex2Range( weekIndex ) { 
        let start = weekIndex * 7;
        return [ start, start + 7 ];
    }
    // Note, calling this with true for the parameter DOES NOT force a save. It only forces an immediate
    // save if there was already a pending save request. If no pending save request, then the entire call is a no-op.
    saveState( commitDelayedSavesNow ) { // conditionally force a save if there was one pending, otherwise a no-op)
        let sym = Symbol.for( "stateManagerSaveStateTimer" );
        let ondelayTimerExpired = () => {
            // Timer expired!
            this[ sym ] = null;
            fs.writeFileSync( FILES[ "state" ], JSON.stringify( this ), { encoding: "utf8" } ); // Only own/enumerable properties (no symbols/methods)
        }
        if ( this[ sym ] ) { // A previous request to save state was made (but is not committed to disk yet)
            clearTimeout( this[ sym ] ); // Cancel the previous [un-expired] timer
            if ( commitDelayedSavesNow ) {
                ondelayTimerExpired(); // Force-save right now (don't wait).
            }
        }
        if ( !commitDelayedSavesNow ) { // Only schedule a save if the commit flag WAS NOT passed.
            // Extend the timer by another 10 seconds (avoid frequest disk writing)
            this[ sym ] = setTimeout( ondelayTimerExpired, 1000 * 10 ); // 10 seconds
        }
    }
}

function monthIndexToDayNameAndWeek( monthIndex, userAlias, someUserAlias ) {
    return `${ dayIndexToName( monthIndex % 7 ) } (${ ( userAlias != someUserAlias ? `${ userMgr.userAliasToName( someUserAlias ) } in ` : "" ) }week ${ parseInt( monthIndex / 7 ) + 1 })`;
}

function dayIndexToName( dayIndex ) {
    switch ( dayIndex ) {
        case 0: return "Sunday";
        case 1: return "Monday";
        case 2: return "Tuesday";
        case 3: return "Wednesday";
        case 4: return "Thursday";
        case 5: return "Friday";
        case 6: return "Saturday";
        default: return "Unknown";
    }
}

class SessionManager {
    constructor() {
        this.sessions = new Map();
    }
    checkSessions() {
        let now = Date.now();
        for ( let [ name, ob ] of this.sessions ) {
            if ( ob.expire < now ) {
                this.sessions.delete( name );
            }
        }
    }
    createSession( responseHeadParam, userAlias ) {
        this.checkSessions(); // Will drop expired sessions.
        let expire = Date.now() + ( 1000 * 60 * 10 ); // +10 minutes
        let sessionKey = `${ (Math.random() * 100000).toFixed() }-${ (Math.random() * 1000).toFixed() }-${ (Math.random() * 1000000).toFixed() }`;
        // if any sessions have this alias active, then extend them, rather than create a new session.
        for ( let [ key, sessionOb ] of this.sessions ) {
            if ( sessionOb.user == userAlias ) {
                sessionKey = key; // use the old key...
            }
        }
        this.sessions.set( sessionKey, { 
            user: userAlias,
            expire: expire
        } );
        responseHeadParam[ "Set-Cookie" ] = `sessionid=${ sessionKey }; Path=/; HttpOnly`;
    }
    getSessionUser( request ) {
        this.checkSessions(); // Will drop expired sessions.
        // Look for the session cookie
        let fullCookie = request.headers.cookie;
        if ( !fullCookie ) {
            return null;
        }
        let variousCookiesBites = fullCookie.split( ";" );
        if ( variousCookiesBites.length > 1 ) {
            return null; // not my cookie!
        }
        let cookieBite = variousCookiesBites[ 0 ];
        let [ name, value ] = cookieBite.split( "=" );
        if ( name != "sessionid" ) {
            return null; // nope, not my cookie!
        }
        if ( !this.sessions.has( value ) ) {
            return null;
        }
        // Extend session by another 10 minutes
        let ob = this.sessions.get( value );
        ob.expire = Date.now() + ( 1000 * 60 * 10 );
        return ob.user;
    }
}

// <key> -- client requested resource name
// method -- client requested resource method (must match)
// creds -- is the requested resource blocked unless the client has logged it?
// res -- file-based resource to return (on the server)
// ct -- content type to use by default for the res resource
// sideEffect -- function to run for custom processing.
//  { request, response, headers, postData, user }
// Note: sideEffect function can override res: value by returning a value, and can override ct (content type) by setting it's own content type on the provided headmeta object
const REQUESTS = {
    "/login"           : { method: "GET",  creds: false, res: "./client/login.html", ct: "text/html" },
    "/client/common.css":{ method: "GET",  creds: false, res: "./client/common.css", ct: "text/css" },
    "/client/utils.js" : { method: "GET",  creds: false, res: "./client/utils.js",  ct: "application/javascript" },
    "/api/loginlist"   : { method: "GET",  creds: false, res: null,                 ct: "application/json", sideEffect: o => {
        let list = [];
        for ( let userData of userMgr ) {
            list.push( userData );
        }
        return JSON.stringify( list, [ "icon", "name", "alias" ] );
    } },
    "/inactive"        : { method: "GET",  creds: false, res: "./client/inactive.html",    ct: "text/html" },
    "/tryagain"        : { method: "GET",  creds: false, res: "./client/try.html",         ct: "text/html" },
    "/notallowed"      : { method: "GET",  creds: false, res: "./client/not-allowed.html", ct: "text/html" },
    "/api/auth"        : { method: "POST", creds: false, res: "./client/confirm.html",     ct: "text/html", sideEffect: o => {
        let authData = fromQueryString( o.postData );
        if ( userMgr[ authData.user ] && userMgr[ authData.user ].pin == authData.pin ) {
            sessionMgr.createSession( o.headers, authData.user );
            if ( userMgr[ authData.user ].admin ) {
                return fs.readFileSync( "./client/confirm-admin.html", { encoding: "utf8" } );
            }
        }
        else {
            return fs.readFileSync( REQUESTS[ "/tryagain" ].res, { encoding: "utf8" } );
        }
    } },
    "/chores"          : { method: "GET",  creds: true,  res: "./client/chores.html", ct: "text/html" },
    "/api/chorelist"   : { method: "GET",  creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        return JSON.stringify( choreMgr, [ "alias", "name", "points" ] );
    } },
    "/api/config"      : { method: "GET",  creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        let config = { user: o.user, chores: choreMgr, submitStatus: stateMgr.getSubmitValues( o.user ), expectedPoints: userMgr[ o.user ].dailyMinPoints };
        return JSON.stringify( config, [ ...Object.keys( config ), "alias", "name", "points" ] );
    } },
    "/api/submitstatus": { method: "GET",  creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        return JSON.stringify( stateMgr.getSubmitMap() );
    } },
    "/api/setsubmitstatus": { method: "POST", creds: "admin", res: null,              ct: "application/json", sideEffect: o => {
        // { user: "name", week: 1, value: 2 }
        return asJSON( o.postData, ( json ) => asUserWeek( json, ( uW ) => JSON.stringify( stateMgr.setSubmitValue( uW.user, uW.week, uW.value ) ) ) );
    } },
    "/api/allstatus"   : { method: "POST", creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        return asJSON( o.postData, ( json ) => asWeek( json, ( week ) => JSON.stringify( stateMgr.getReportList( week, o.user ) ) ) );
    } },
    "/api/choreselect" : { method: "POST", creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        // [ { select: bool, chore: string, week: #, day: # } ] - further validated in 'selectChores'
        return asJSON( o.postData, ( json ) => asList( json, ( list ) => JSON.stringify( stateMgr.selectChores( o.user, list ) ) ) );
    } },
    "/api/submitchores": { method: "POST", creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        return asJSON( o.postData, ( json ) => asWeek( json, ( week ) => JSON.stringify( stateMgr.submitChores( o.user, week ) ) ) );
    } },
    "/api/selectedweek": { method: "POST", creds: true,  res: null,                   ct: "application/json", sideEffect: o => {
        return asJSON( o.postData, ( json ) => asWeek( json, ( week ) => JSON.stringify( stateMgr.getSelectedChoresForWeek( o.user, week ) ) ) );
    } },
    "/printone"        : { method: "GET",  creds: true,  res: "./client/print-individual.html", ct: "text/html" },
    "/api/choremap"    : { method: "GET",  creds: "admin", res: null,                   ct: "application/json", sideEffect: o => {
        return JSON.stringify( stateMgr.getChoreMapSimple() );
    } },
    "/api/availabilties":{ method: "GET",  creds: "admin", res: null,                   ct: "application/json", sideEffect: o => {
        return JSON.stringify( availMgr.getAvailabilityMapSummary() );
    } },
    "/api/groups"      : { method: "GET",  creds: "admin", res: null,                   ct: "application/json", sideEffect: o => {
        return JSON.stringify( groupMgr.getGroupMapSummary() );
    } },
    "/admin"           : { method: "GET",  creds: "admin", res: "./client/admin.html",     ct: "text/html" },
    "/admin/map"       : { method: "GET",  creds: "admin", res: "./client/chore-map.html", ct: "text/html" },
    "/api/stateselect" : { method: "POST", creds: "admin", res: null,                      ct: "application/json", sideEffect: o => {
        // { chore: 'xxx', changeto: { availId|groupId: 'xxxx'|null } }
        return asJSON( o.postData, ( json ) => JSON.stringify( stateMgr.setChoreMap( json ) ) );
    } },
    "/admin/approve"   : { method: "GET",  creds: "admin", res: "./client/chore-approve.html", ct: "text/html" },
    "/api/weekreview"  : { method: "POST", creds: "admin", res: null,                      ct: "application/json", sideEffect: o => {
        // { user: 'xx', week: ## }
        return asJSON( o.postData, ( json ) => asUserWeek( json, ( req ) => {
            let weekReview = { 
                expectedPoints: userMgr[ req.user ].dailyMinPoints, 
                choreStates: stateMgr.getReportListSelected( req.week, req.user ),
                weekChoreAliases: stateMgr.getSelectedChoresForWeek( req.user, req.week ) 
            };
            return JSON.stringify( weekReview, [ ...Object.keys( weekReview), "alias", "errorReason", "unavailableReason", "incompleteReason" ] );
         } ) );
    } },
    "/api/clearchores" : { method: "POST", creds: "admin", res: null,                      ct: "application/json", sideEffect: o => {
        // { user: 'xx', week: ## }
        return asJSON( o.postData, ( json ) => asUserWeek( json, ( ob ) => JSON.stringify( stateMgr.clearChoreWeek( ob.user, ob.week ) ) ) );
    } },
    "/api/clearchoresweek": { method: "POST", creds: "admin", res: null,                   ct: "application/json", sideEffect: o => {
        return asJSON( o.postData, ( json ) => asWeek( json, ( week ) => JSON.stringify( stateMgr.clearChoreWeekPerWeek( week ) ) ) );
    } },
    "/api/clearchoresuser": { method: "POST", creds: "admin", res: null,                   ct: "application/json", sideEffect: o => {
        return asJSON( o.postData, ( json ) => asUser( json, ( user ) => JSON.stringify( stateMgr.clearChoreWeekPerUser( user ) ) ) );
    } },
    "/api/clearchoresall" : { method: "GET",  creds: "admin", res: null,                   ct: "application/json", sideEffect: o => {
        return JSON.stringify( stateMgr.clearChoreWeekAll() );
    } }
};
// TODO: rename certain APIs to "MY" prefix when using the implied ambient user cookie.

async function serverResponseCallback( request, response ) {
    // Slow network
    if ( simulatorSpeed ) {
        await slowNetworkSimulator();
    }
    // Try to get an active session from a session cookie...
    let sessionUser = sessionMgr.getSessionUser( request ); // returns null for no associated session.
    // Try to get POST data if applicable...
    let posted = null;
    if ( request.method == "POST" ) {
        try {
            posted = await getBody( request );
        }
        catch ( err ) {
            response.writeHead( 413, {'Content-Type': 'text/plain'} ); // request too large!
            response.end();
            request.connection.destroy();
            return;
        }
    }
    // Request validation - URL must be a known URL with expected method.
    if ( !REQUESTS[ request.url ] || ( REQUESTS[ request.url ].method != request.method ) ) {
        response.writeHead( 404, { "Content-Type" : "text/html" } );
        response.write("<h1>Chore Application Error</h1>");
        response.write(`<p>Request for ${ request.url } with method ${ request.method } not valid.</p>` );
        response.write(`<p>Try <a href="/login">logging in again</a></p>`)
        response.end();
        return;
    }
    let contextOb = REQUESTS[ request.url ];
    // Request validation - if the resource needs a session to be released, but no session could be loaded, request login.
    if ( contextOb.creds && !sessionUser ) {
        response.writeHead( 200, { "Content-Type" : REQUESTS[ "/inactive" ].ct } );
        response.write( fs.readFileSync( REQUESTS[ "/inactive" ].res, { encoding: "utf8" } ) );
        response.end();
        return;
    }
    // Admin resources can only be released to admins
    if ( contextOb.creds && contextOb.creds == "admin" && ( !sessionUser || !userMgr[ sessionUser ].admin ) ) {
        response.writeHead( 200, { "Content-Type" : REQUESTS[ "/notallowed" ].ct } );
        response.write( fs.readFileSync( REQUESTS[ "/notallowed" ].res, { encoding: "utf8" } ) );
        response.end();
        return;
    }
    // Get resources.
    let headMeta = {};
    let customReturnVal = undefined;
    if ( contextOb.sideEffect ) {
        customReturnVal = contextOb.sideEffect( { 
            request: request, 
            response: response, 
            headers: headMeta, 
            postData: posted,
            user: sessionUser 
        } );
    }
    if ( !headMeta[ "Content-Type" ] ) {
        headMeta[ "Content-Type" ] = contextOb.ct;
    }
    response.writeHead( 200, headMeta );
    if ( customReturnVal ) {
        response.write( customReturnVal );
    }
    else if ( contextOb.res ) {
        response.write( fs.readFileSync( contextOb.res, { encoding: "utf8" } ) );
    }
    response.end();
}

function getBody( request ) {
    let body = "";
    return new Promise( ( resolveFunc, rejectFunc ) => {
        request.on( "data", ( chunk ) => {
            body += chunk;
            // DoD
            if (body.length > 1e6) {
                rejectFunc("body too large (exceeded 1e6 in size)");
            }
        } );
        request.on( "end", () => {
            resolveFunc( body );
        } );
    } );
}

function fromQueryString( qs ) {
    let ob = {};
    for ( let part of qs.split( "&" ) ) {
        let [ name, val ] = part.split( "=" );
        ob[ name ] = val;
    }
    return ob;
}

function asJSON( postData, nextStepFunc ) {
    let json = null;
    try {
        json = JSON.parse( postData );
    }
    catch ( ex ) {
        return JSON.stringify( { error: `expected JSON POST data; got invalid JSON: ${ ex }` } );
    }
    return nextStepFunc( json );
}
// Extracts the week index (and verifies it) from postData, then (if successful) 
// calls the nextStepFunc with the week index, returning whatever it returns.
function asWeek( json, nextStepFunc ) {
    let { week } = json;
    if ( typeof week != "number" || week < 0 || week > 3 ) {
        return JSON.stringify( { error: "expected POST data of the form { week: # } where # is a number between 0 and 3." } );
    }
    return nextStepFunc( week );
}

function asUser( json, nextStepFunc ) {
    let { user } = json;
    if ( typeof user != "string" || !userMgr[ user ] ) {
        return JSON.stringify( { error: "expected POST data of the form { user: 'xx' } where 'xx' is a valid user alias." } );
    }
    return nextStepFunc( user );
}

function asList( json, nextStepFunc ) {
    if ( !Array.isArray( json ) ) {
        return JSON.stringify( { error: "expected POST data to be a list of objects [ {}, ... ]." } );
    }
    return nextStepFunc( json );
}

// Ensures the JSON is an object with user + week values
function asUserWeek( json, nextStepFunc ) {
    let { user, week } = json;
    if ( typeof user != "string" || !userMgr[ user ] ) {
        return JSON.stringify( { error: "expected POST data to be an object with a 'user' key whose value is a known user." } );
    }
    if ( typeof week != "number" || week < 0 || week > 3 ) {
        return JSON.stringify( { error: "expected POST data to be an object with a 'week' key whose value is a number between 0 and 3." } );
    }
    return nextStepFunc( json );
}

function slowNetworkSimulator() {
    return new Promise( ( resolve ) => setTimeout( resolve, simulatorSpeed ) );
}

function saveStateOnExit() {
    process.on( "exit", ( code ) => {
        stateMgr.saveState( true );
    } );
}

ensureDirs();
const userMgr = new UserManager();
const groupMgr = new GroupManager(); // Requires userMgr defined.
const choreMgr = new ChoreManager();
const availMgr = new AvailabilityManager();
loadOptionalCustomExtension();
const stateMgr = new StateManager();
const sessionMgr = new SessionManager();
const simulatorSpeed = null; // SLOW_NETWORK.CELL3G; // or null
const server = http.createServer( serverResponseCallback );
server.listen( { port: 80 } );
saveStateOnExit();

console.log( "Server started on port 80." );
console.log( "  Point browser to: http://localhost/login" );